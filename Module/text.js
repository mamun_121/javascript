// By separating code with similar logic into files called modules, 

// 
// find, fix, and debug code more easily;
//reuse and recycle defined logic in different parts of our application;
// keep information private and protected from other modules;
// and, importantly, prevent pollution of the global namespace and potential naming collisions,
// by cautiously selecting variables and behavior we load into a program.

// As of ES6, default export and named exports. 
// When using ES6 syntax, we use export default in place of module.exports.